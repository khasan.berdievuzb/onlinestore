package io.my.orderservice.command.api.controller;

import io.my.orderservice.command.api.command.CreateOrderCommand;
import io.my.orderservice.command.api.model.OrderRestModel;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/orders")
public class OrderCommandController {


    private CommandGateway commandGateway;

    public OrderCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public String createOrder(@RequestBody OrderRestModel orderRestModel){

        CreateOrderCommand createOrderCommand=CreateOrderCommand.builder()
                .orderId(UUID.randomUUID().toString())
                .productId(orderRestModel.getProductId())
                .addressId(orderRestModel.getAddressId())
                .userId(orderRestModel.getUserId())
                .quantity(orderRestModel.getQuantity())
                .orderStatus("CREATED")
                .build();
        commandGateway.sendAndWait(createOrderCommand);

        return "order created";
    }

    public String updateOrder(){
        return "order updated";
    }
}
